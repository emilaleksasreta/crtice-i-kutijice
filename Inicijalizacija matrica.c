#include<stdio.h>
#include<stdlib.h>


/*Funkcija alocira kvadratnu matricu i popuni je short-ovima (1111). Svaka cifra predstavlja stranicu polja (levo, gore, desno, dole) i ako je 1, znaci
da je prazno, ako je 2, znaci da je crtica povucena*/
short** kvadratna_tabla(int m, int n)
{
	short **tablaKv;
	int i, j;

	tablaKv = (short**)malloc(m*sizeof(short*));
	for(i=0; i<m; i++)
		tablaKv[i] = (short*)malloc(n*sizeof(short));

	for(i=0; i<m; i++)
		for(j=0; j<n; j++)
			tablaKv[i][j] = 1111;
	
	return tablaKv;
}


/*Funkcija alocira trougaonu matricu i popuni je short-ovima. (111). Svaka cifra predstavlja stranicu polja (levo, gore, desno, dole) i ako je 1, znaci 
da je prazno, ako je 2, znaci da je crtica povucena. Ako je 'j' koordinata polja parna, polje ima donjeg suseda, ako je neparna onda ima gornjeg*/
short** trougaona_tabla(int m)
{
	short **tablaTr;
	int i, j;

	tablaTr = (short**)malloc(m*sizeof(short*));
	for (i=0; i<m; i++)
		tablaTr[i] = (short*)malloc((i*2+1)*sizeof(short)); //U svakoj vrsti ima i*2+1 kolona

	for(i=0; i<m; i++)
		for(j=0; j<(i*2+1); j++)
			tablaTr[i][j] = 111;

	return tablaTr;
}

//Testiranje
void main()
{
	short **tablaKv, **tablaTr;
	int m, n, i, j;

	printf("Uneti dimenzije kvadratne\n");
	scanf("%d%d", &m, &n);
	tablaKv = kvadratna_tabla(m, n);

	for(i=0; i<m; i++)
	{
		for(j=0; j<n; j++)
			printf("%hd ", tablaKv[i][j]);
		printf("\n");
	}
	printf("==============================\n");

	printf("Uneti dimenzije trougaone\n");
	scanf("%d", &m);
	tablaTr = trougaona_tabla(m);

	for(i=0; i<m; i++)
	{
		for(j=0; j<(i*2+1); j++)
			printf("%hd ", tablaTr[i][j]);
		printf("\n");
	}
}


	