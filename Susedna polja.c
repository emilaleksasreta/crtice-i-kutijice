#include <stdio.h>
#include <stdlib.h>

//Struktura polje je sastavni deo strukture Sused. U nju se upisuju koordinate odredjenog polja.
typedef struct
{
	int i, j;
} Polje;

/*Struktura Sused oznacava sve susede datom polju. U slucaju da je trougaona matrica, polje ima gornjeg suseda ako je koordinata j neparna, a
donjeg ako je parna*/
typedef struct
{
	Polje levo, gore, desno, dole;
} Sused;


/*Funkcija prima koordinate polja za koje se traze susedi kao i dimenzije cetvorougaone table. Funkcija vraca strukturu Sused
u koju su upisane koordinate suseda. Ako polje nema odredjenog suseda (npr desnog), upisuje se -1 kao 'i' koordinata tog suseda*/
Sused sused_kvadrat(int i, int j, int m, int n)
{
	Sused susedi;

	if (j-1 >= 0) 
		susedi.levo.i = i, susedi.levo.j = j-1;
	else 
		susedi.levo.i = -1;
	
	if (i-1 >= 0) 
		susedi.gore.i = i-1, susedi.gore.j = j;
	else 
		susedi.gore.i = -1;
	
	if (j+1 < n)
		susedi.desno.i = i, susedi.desno.j = j+1;
	else
		susedi.desno.i = -1;

	if (i+1 < m)
		susedi.dole.i = i+1, susedi.dole.j = j;
	else
		susedi.dole.i = -1;
	
	return susedi;
}


/*Funkcija prima koordinate polja za koje se traze susedi kao i dimenziju trougaone table. Funkcija vraca strukturu Sused
u koju su upisane koordinate suseda. Ako polje nema odredjenog suseda (npr desnog), upisuje se -1 kao 'i' koordinata tog suseda*/
Sused sused_trougao(int i, int j, int m)
{
	Sused susedi;

	if (j-1 >= 0)
		susedi.levo.i = i, susedi.levo.j = j-1;
	else
		susedi.levo.i = -1;

	if (j+1 <= i*2)
		susedi.desno.i = i, susedi.desno.j = j+1;
	else
		susedi.desno.i = -1;

	if (j%2 == 0)
	{
		susedi.gore.i = -1;
		if (i+1 < m)
			susedi.dole.i = i+1, susedi.dole.j = j+1;
		else
			susedi.dole.i = -1;
	}
	else
	{
		susedi.dole.i = -1;
		if (i-1 >= 0)
			susedi.gore.i = i-1, susedi.gore.j = j-1;
		else
			susedi.gore.i = -1;
	}

	return susedi;
}

	
//Testiranje
void main()
{
	Sused polje;
	int m, n;
	int i, j;

	printf("Ucitati dimenzije cetvorougaone matrice\n");
	scanf("%d%d", &m, &n);
	printf("Ucitati polje za koje se traze susedi\n");
	scanf("%d%d", &i, &j);
	polje = sused_kvadrat(i, j, m, n);
	if (polje.levo.i != -1)
		printf("%d %d\n", polje.levo.i, polje.levo.j);
	if (polje.gore.i != -1)
		printf("%d %d\n", polje.gore.i, polje.gore.j);
	if (polje.desno.i != -1)
		printf("%d %d\n", polje.desno.i, polje.desno.j);
	if (polje.dole.i != -1)
		printf("%d %d\n", polje.dole.i, polje.dole.j);
	printf("==================\n");

	printf("Ucitati dimenziju trougaone matrice\n");
	scanf("%d", &m);
	printf("Ucitati polje za koje se traze susedi\n");
	scanf("%d%d", &i, &j);
	polje = sused_trougao(i, j, m);
	if (polje.levo.i != -1)
		printf("%d %d\n", polje.levo.i, polje.levo.j);
	if (polje.gore.i != -1)
		printf("%d %d\n", polje.gore.i, polje.gore.j);
	if (polje.desno.i != -1)
		printf("%d %d\n", polje.desno.i, polje.desno.j);
	if (polje.dole.i != -1)
		printf("%d %d\n", polje.dole.i, polje.dole.j);
	printf("==================\n");
}


	
	