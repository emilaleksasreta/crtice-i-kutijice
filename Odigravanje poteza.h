#include <stdio.h>
#include <stdlib.h>

#ifndef __Odigravanja_poteza_H
#define __Odigravanje_poteza_h
#endif


typedef struct
{
	int i, j;
} Polje;

typedef struct
{
	Polje levo, gore, desno, dole;
} Sused;

short levoKv(short n);
short goreKv(short n);
short desnoKv(short n);
short doleKv(short n);

short levoTr(short n);

short gore_doleTr(short n);

short desnoTr(short n);


Sused sused_kvadrat(int i, int j, int m, int n);

Sused sused_trougao(int i, int j, int m);


short** kvadratna_tabla(int m, int n);

short** trougaona_tabla(int m);

/*Funkcija koja menja stanje kvadratne matrice kad se odigra potez.
Prima kvadratnu matricu, dimenzije, koordinate polja i broj od 1 do 4 koji oznacava stranicu polja
Vraca 1 ako je bilo moguce odigrati potez, a 0 ako je nemoguce.*/
int odigraj_potez_kv(short **tabla, int m, int n, int i, int j, int str);

/*Funkcija koja menja stanje trougaone matrice kad se odigra potez.
Prima trougaonu matricu, dimenziju, koordinate polja i broj od 1 do 3 koji oznacava stranicu polja
Vraca 1 ako je bilo moguce odigrati potez, a 0 ako je nemoguce.*/
int odigraj_potez_tr(short **tabla, int m, int i, int j, int str);