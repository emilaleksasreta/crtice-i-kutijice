#include <curses.h>
#include <stdlib.h>
#include <windows.h>

#ifndef __meni_H
#define __meni_H
#endif


#define GLAVNI_MENI 10
#define PODESAVANJA 11
#define HIGHSCORE 12
#define TEZINA 13
#define VELICINA_MAPE 14
#define VRSTA_MAPE 15
#define PAUZA 16
#define IZLAZAK 20

#define meni(x,n) w_meni(stdscr,x,n)
#define l(s) strlen(s)

#define NCURSES_MOUSE_VERSION







void pisi_meni(WINDOW*, char**, int , int , int , int);

void sel_opc(WINDOW* win, int y, int max_x, int min_x);


/*Funkcija ispisuje meni iz niza stringova opcije, duzine n
* u prozoru win. Nulti string u nizu je naslov menija
* Potom ceka da korisnik tastaturom ili misem
* izavere jednu opciju i vraca redni broj izabrane opcije*/
int w_meni(WINDOW* win, char** opcije, int n);

