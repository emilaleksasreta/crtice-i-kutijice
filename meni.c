#include "meni.h"


#define in(a, x, y) (x<a && a<y)


void pisi_meni(WINDOW  *win, char **opcije, int n, int odabran, int maxx, int maxy)
{
	int i; //brojac
	wattron(win, A_BOLD);
	mvwaddstr(win,1, (maxx-l(opcije[0]))/2, opcije[0]);										// Ispisuje naziv menija
	wattroff(win, A_BOLD);
	mvwaddstr(win,maxy-1, (maxx-l(opcije[n-1]))/2, opcije[n-1]);							// Ispisuje povratnu/izlaznu opciju

	for (i=1; i<n-1;i++)
		mvwaddstr(win,maxy/2 + (2*i - n),(maxx-l(opcije[i]))/2, opcije[i]);					// Ispisuje ostale opcije
	if (odabran)
	sel_opc(win, maxy/2 + 2 - n, (maxx + l(opcije[1]))/2, (maxx - l(opcije[1]))/2);

	refresh();
}

void sel_opc(WINDOW* win, int y, int max_x, int min_x)
{
	int k = max_x + 1 - min_x;
	mvwaddch(win, y, min_x - 2, ACS_RARROW | A_BOLD);		
	mvwaddch(win, y, max_x + 2, ACS_LARROW | A_BOLD);
	mvwchgat(win, y, min_x , k, A_BOLD, 7, NULL);
	refresh();
}


int w_meni(WINDOW* win, char** opcije, int n)
{
	int maxy, maxx,		// velicina ekrana
		i,j,k,			// brojaci
		odabran = 1;	// selektovana opcija
	int key;

	static MEVENT klik;

	wbkgd(win, COLOR_PAIR(7));


	keypad(win, TRUE);

	getmaxyx(win, maxy, maxx);
	wclear(win);
	pisi_meni(win, opcije, n, odabran, maxx, maxy);

	// Cekanje odabira opcije

	while (1)
	{
		wrefresh(win);
		key = wgetch(win);
		switch(key)
		{
		case KEY_UP : case 'w': case 'W':
			if (odabran > 1)
			{
				if (odabran == n-1 && opcije[n-1] != "")
				{	//Ako je prethodna opcija bila poslednja, (i ona postoji) uklanja strelice na dnu ekrana)

					mvwchgat(win, maxy-1, (maxx-l(opcije[n-1]))/2 - 2, l(opcije[n-1]) + 5, COLOR_PAIR(7), 0, NULL);
					mvwaddch(win, maxy-1, (maxx-l(opcije[n-1]))/2 - 2, ' ');					
					mvwaddch(win, maxy-1, (maxx+l(opcije[n-1]))/2 + 2, ' ');					
				}
				else
				{	//Ako ne, uklanja strelice na sredini ekrana

					mvwchgat(win, maxy/2 - (n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, l(opcije[odabran]) + 5, COLOR_PAIR(7), 0, NULL);
					mvwaddch(win, maxy/2-(n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, ' ');
					mvwaddch(win, maxy/2-(n-2*odabran), (maxx+l(opcije[odabran]))/2 + 2, ' ');	

				}

				odabran--;	// Promeni odabir

				sel_opc(win, maxy/2 +2*odabran - n, (maxx + l(opcije[odabran]))/2, (maxx - l(opcije[odabran]))/2);
			}
			else
			{};
			break;
		case KEY_DOWN : case 's' : case 'S' :
			if (odabran < n - 1)
			{	// Uklanjanje strelica
				mvwchgat(win, maxy/2 - (n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, l(opcije[odabran]) + 5, COLOR_PAIR(7), 0, NULL);
				mvwaddch(win, maxy/2-(n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, ' ');
				mvwaddch(win, maxy/2-(n-2*odabran), (maxx+l(opcije[odabran]))/2 + 2, ' ');

				odabran++; // Promena odabira

				
				if (odabran == n-1)
				{	
					if (opcije[n-1] != "")
					//Ako je selektovana poslednja opcija, (i ona postoji) crta strelice na dnu ekrana)
					sel_opc(win, maxy - 1, (maxx + l(opcije[odabran]))/2, (maxx - l(opcije[odabran]))/2);
					else
					//Vrati odabir na prethodnu opciju. (Da ne bi bila odabrana nepostojeca opcija)
					odabran--;

				}
				else
				{	//Ako ne, crta strelice na sredini ekrana
					sel_opc(win, maxy/2 +2*odabran - n, (maxx + l(opcije[odabran]))/2, (maxx - l(opcije[odabran]))/2);
				}
			}
			else
			{};
			break;
		case 10: // pritisnut enter
			wrefresh(win);
			return odabran;
			break;
		case KEY_MOUSE :
			if (nc_getmouse(&klik) == OK)
			{
				//mvwaddch(win, 0, 0, 'g'|COLOR_PAIR(2));
				if (klik.bstate & BUTTON1_CLICKED) // Kliknut mis
				{
					if ((klik.x - (*win)._begx  <=  maxx/2+l(opcije[n-1])) && (klik.x - (*win)._begx >= maxx/2-l(opcije[n-1])) && (klik.y - (*win)._begy + 1 == maxy))
					{	// Proverava da li je kliknuta
						// povratna opcija
						//wrefresh(win);
						return n-1;
					}
					else 
					for (i=1; i<n-1;i++)
					if ((klik.x - (*win)._begx <=  (maxx+l(opcije[i]))/2) && (klik.x - (*win)._begx >= (maxx-l(opcije[i]))/2) && (klik.y - (*win)._begy == maxy/2 + 2*i - n))
					{	// Proverava da li je kliknuta
						//	neka od ostalih opcija
						
						wrefresh(win);
						return i;
					}
				}
				if (klik.bstate & PDC_MOUSE_MOVED)
				{
					if ((klik.x - (*win)._begx <=  maxx/2+l(opcije[n-1])) && (klik.x - (*win)._begx >= maxx/2-l(opcije[n-1])) && (klik.y - (*win)._begy + 1 == maxy))
					{
						if (opcije[n-1] != "")
						{
						mvwchgat(win, maxy/2 - (n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, l(opcije[odabran]) + 5, COLOR_PAIR(7), 0, NULL);
						mvwaddch(win, maxy/2-(n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, ' ');
						mvwaddch(win, maxy/2-(n-2*odabran), (maxx+l(opcije[odabran]))/2 + 2, ' ');
						odabran = n-1;
						sel_opc(win, maxy - 1, (maxx + l(opcije[odabran]))/2, (maxx - l(opcije[odabran]))/2);
						}
					}
					else
					{
						for (i=1; i<n-1;i++)
						if ((klik.x - (*win)._begx <=  (maxx+l(opcije[i]))/2) && (klik.x - (*win)._begx >= (maxx-l(opcije[i]))/2) && (klik.y - (*win)._begy == maxy/2 + 2*i - n)) 
						{	// proverava da li je kliknuta
							//	neka od ostalih opcija
							if (odabran == n-1)
							{
								mvwchgat(win, maxy-1, (maxx-l(opcije[n-1]))/2 - 2, l(opcije[n-1]) + 5, COLOR_PAIR(7), 0, NULL);
								mvwaddch(win, maxy-1, (maxx-l(opcije[n-1]))/2 - 2, ' ');					
								mvwaddch(win, maxy-1, (maxx+l(opcije[n-1]))/2 + 2, ' ');
								
							}
							else
							{
								mvwchgat(win, maxy/2 - (n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, l(opcije[odabran]) + 5, COLOR_PAIR(7), 0, NULL);
								mvwaddch(win, maxy/2-(n-2*odabran), (maxx-l(opcije[odabran]))/2 - 2, ' ');
								mvwaddch(win, maxy/2-(n-2*odabran), (maxx+l(opcije[odabran]))/2 + 2, ' ');
							}
							odabran = i;
							sel_opc(win, maxy - 1, (maxx + l(opcije[odabran]))/2, (maxx - l(opcije[odabran]))/2);
						}
					}
				}
			}
			break;
			
		}
		
	}
	
}
