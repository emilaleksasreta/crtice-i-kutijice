#include <stdio.h>

//Funkcije ocekuju cetvorocifreni short i vracaju cifru iz broja
short levoKv(short n)
{
	return n / 1000;
}

short goreKv(short n)
{
	return (n / 100) % 10;
}

short desnoKv(short n)
{
	return (n / 10) % 10;
}

short doleKv(short n)
{
	return n % 10;
}

//Funkcije ocekuju trocifreni short
short levoTr(short n)
{
	return n / 100;
}

short gore_doleTr(short n)
{
	return (n / 10) % 10;
}

short desnoTr(short n)
{
	return n % 10;
}

//Testiranje
void main()
{
	int n;
	printf("Uneti cetvorocifreni broj.\n");
	scanf("%hd", &n);
	printf("%hd %hd %hd %hd\n", levoKv(n), goreKv(n), desnoKv(n), doleKv(n));
	printf("=================\n");
	printf("Uneti trocifreni broj.\n");
	scanf("%hd", &n);
	printf("%hd %hd %hd\n", levoTr(n), gore_doleTr(n), desnoTr(n));
}