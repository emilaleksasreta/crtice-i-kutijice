#include "meni.h"
#include "Odigravanje poteza.h"

#define IGRAC_1 0
#define IGRAC_2 1
#define MAGENTA 2
#define	CIJAN 3
#define	ZUTA 4
#define	ZELENA 5
#define	CRVENA 6
#define	CRNA 7


typedef struct
{
	int x, y, i;
} Crtica;


void intro_scr();
void ispisi_naslov(char* naslov);

void change_scr_size(int Height, int Width);

int inic_meni(WINDOW* menu_win, int meni);

int inic_meni(WINDOW* menu_win, int meni);

WINDOW *iscr_kv_tablu(int n, int m);

Crtica koord_crtice(int poz_x, int poz_y, int sir_table, int vis_table);

char naslov[] = "PROJEKAT CRTICE",
	*glavni_meni[] = {	"Dobro dosli!", "Igra protiv racunara","Igra za dva igraca",
							"Nastavi sacuvanu partiju", "Najbolji rezultati",
							"Podesavanja", "Izadji"},
	*podesavanja[] = { "Podesavanja", "Tezina" , "Velicina mape", "Vrsta mape", "Nazad"},
	*tezine[] = {"Tezina kompjuterskog protivnika","Lako", "Srednje", "Tesko", "Nazad"},
	*pauza[] = {"Pauza", "Nastavi", "Sacuvaj partiju", "Ucitaj sacuvanu partiju", "Pomoc", "Izadji"},
	*vrsta_mape[] = {"Tip mape", "Pravougaona", "Trougaona", "Nazad"},
	*velicina_kv[] = {"Velicina mape", "5x5", "5x10", "5x15", "10x10", "10x15", "15x15", "15x20", "Nazad"},
	*velicina_tr[] = {"Velicina mape", "visina - 5", "visina - 10", "visina - 15", "visina - 20", "Nazad"},
	*rezultati[] = {"Najbolji rezultati", 
					"1)---------------- : ---", "2)---------------- : ---",
					"3)---------------- : ---", "4)---------------- : ---",
					"5)---------------- : ---", "6)---------------- : ---",
					"7)---------------- : ---", "8)---------------- : ---",
					"9)---------------- : ---", "10)---------------- : ---",
					""},
	*Pobedio2[] = {"POBEDIO JE IGRAC 2!", "Glavni meni", "Izlaz"},
	*Pobedio1[] = {"POBEDIO JE IGRAC 1!", "Glavni meni", "Izlaz"},
	*PobedioP[] = {"POBEDILI STE!", "Glavni meni", "Izlaz"},
	*PobedioK[] = {"IZGUBILI STE...", "Glavni meni", "Izlaz"};

int kvadratna = 1, mapa_kv = 4, mapa_tr = 2, tezina = 2;
int igrac = 0, igrac1 = 0, igrac2 = 0, osvojena_polja = 0;
int sirina = 5, visina = 5;

void main()
{
	short** Tabla;
	int i, j, rows, cols;

	WINDOW *tabla, *meniji;
	
	MEVENT klik;

	change_scr_size(45, 80);
	initscr();
	
	noecho();	cbreak();	curs_set(0);	start_color();
	resize_term(45, 80);

	init_pair(0, COLOR_RED, COLOR_GREEN);
	init_pair(1, COLOR_BLUE, COLOR_GREEN);
	init_pair(6, COLOR_RED, COLOR_GREEN);
	init_pair(5, COLOR_WHITE, COLOR_GREEN);
	init_pair(4, COLOR_YELLOW, COLOR_GREEN);
	init_pair(3, COLOR_CYAN, COLOR_BLUE);
	init_pair(2, COLOR_MAGENTA, COLOR_BLUE);
	init_pair(7, COLOR_RED | COLOR_YELLOW, COLOR_BLUE);

	mousemask(ALL_MOUSE_EVENTS, NULL);
	
	bkgd(COLOR_PAIR(7));
	intro_scr();

	ispisi_naslov(naslov);

	meniji = newwin(25, 30, 10, 25);
	
	while(1)
	{
		i = inic_meni(meniji, GLAVNI_MENI);
	
		
		wclear(meniji);
		wrefresh(meniji);
		tabla = iscr_kv_tablu(visina, sirina);
		Tabla = kvadratna_tabla(visina, sirina);
		while (osvojena_polja - 25)
		{
			if (cekaj_potez(tabla, igrac, Tabla)) igrac = igrac?0:1;
			mvprintw(1,1,"%d  ", osvojena_polja);
			refresh();
		}
		wrefresh(tabla);

		refresh();
		wgetch(tabla);
		getch();
		if (igrac2 > igrac1)
		{
			i = w_meni(meniji, Pobedio2, 3);
		}
		else i = w_meni(meniji, Pobedio1, 3);
		if (i == 2) exit(0);
		
	}
	endwin();
}

//Ispisuje naslov na vrhu ekrana
void ispisi_naslov(char* naslov)
{
	int i,j,k=l(naslov),y,x;

	attron(A_BOLD | COLOR_PAIR(4));

	getmaxyx(stdscr, y, x);
	for (i=0; i< k; i++)
		mvaddch(1, (x-k)/2 +i, naslov[i]);
	for (i=0; i< k; i++)
		mvaddch(2, (x-k)/2 + i, ACS_HLINE);


	attroff(A_BOLD | COLOR_PAIR(4));
	refresh();
}

// ispisuje intro poruku

void intro_scr()
{
	int lenght, i, j, x, y;
	char	weltex[] = "Welcome, human!",
			pocetak[] = "Are you prepared to conquer the world?";
	clear();
	halfdelay(1);
	attron(COLOR_PAIR(CRVENA));
	lenght = l(weltex);
	for (i = 0; i < lenght; i++)
	{
		move (LINES/2 - 1, (COLS-i-1)/2);
		for (j=0; j<=i; j++)
			addch(weltex[j]);
		addch(ACS_BLOCK);
		if (getch() != ERR)
		{
			mvaddstr(LINES/2 - 1, (COLS-lenght)/2, weltex);
			addch(ACS_BLOCK);
			break;
		}
	}
	getyx(stdscr, y, x);
	mvaddch(y, x-1, ' ');
	halfdelay(10);	getch();
	halfdelay(1);
	lenght = l(pocetak);
	attron(COLOR_PAIR(ZUTA) | A_BOLD);
	for (i = 0; i < lenght; i++)
	{
		move (LINES/2 + 2, (COLS-i-1)/2);
		for (j=0; j<=i; j++)
			addch(pocetak[j]);
		addch(ACS_BLOCK);
		if (getch() != ERR)
		{
			mvaddstr(LINES/2 +2, (COLS-lenght)/2, pocetak);
			addch(ACS_BLOCK);
			break;
		}
	}
	getyx(stdscr, y, x);
	mvaddch(y, x-1, ' ');
	halfdelay(20);	getch();
	nocbreak();
	cbreak();
	attroff(COLOR_PAIR(ZUTA) | A_BOLD);
	clear();
}

// Crta pocetnu poziciju na tabli zadatih dimenzija

WINDOW *iscr_kv_tablu(int n, int m)
{
	int x = m*2+5, y = n*2+5;	//dimenzije
	int starty = (LINES - y) / 2, startx = (COLS - x) / 2; //centriranje novog prozora
	int i, j; //brojaci
	WINDOW *win;
	if (x > COLS || y > LINES) return NULL;
	win = newwin(y,x, starty, startx);
	wbkgd(win, COLOR_PAIR(4));
	wattron(win, A_BOLD);
	wborder(win, '#','#','#','#','#','#','#','#');
	wattroff(win, A_BOLD);
	x = 2; y = 2;
	wmove(win, y++, x);
	
	for (i=0; i<m;i++)
	{	
		waddch(win,  ACS_PLUS | A_BOLD | COLOR_PAIR(5));
		waddch(win, ACS_HLINE | COLOR_PAIR(6)); }
		waddch(win,  ACS_PLUS | A_BOLD | COLOR_PAIR(5));
	for (i=0;i<n;i++)
	{

		wmove(win, y++, x);
		for (j=0;j<m;j++)
		{	
			waddch(win, ACS_VLINE | COLOR_PAIR(6));
			waddch(win, ACS_BLOCK | COLOR_PAIR(4));
		}
		waddch(win, ACS_VLINE | COLOR_PAIR(6));
		wmove(win, y++, x);
		for (j=0;j<m;j++)
		{	
			waddch(win, ACS_PLUS | A_BOLD | COLOR_PAIR(5));
			waddch(win, ACS_HLINE | COLOR_PAIR(6));
		}
		waddch(win, ACS_PLUS | A_BOLD | COLOR_PAIR(5));
	}
	wrefresh(win);
	return win;
}

/*Menja velicinu terminala na zadate dimenzije
*  Potrebno posle ove funkcije promeniti velicinu
*  stdscr kada se pokrene curses mod*/
void change_scr_size(int Height, int Width)
{
	HANDLE hOut; //Windows output handle
	SMALL_RECT newScreenSize; //Rectangle containing the new dimensions

	//Defining the dimensions of our window
	newScreenSize.Left = 0; //(Upper left)
	newScreenSize.Right = Width; //(Bottom right)
	newScreenSize.Top = 0;
	newScreenSize.Bottom = Height; //Height of window
	hOut = GetStdHandle(STD_OUTPUT_HANDLE); //Loading the handle
	SetConsoleWindowInfo(hOut, TRUE, &newScreenSize); //Actually setting it
	//The true is for absolute dimensions vs. relative to the old dimensions

	//Show off our pretty window
}

/*Racunanje koordinata crtice koju
* je korisnik kliknuo*/
Crtica koord_crtice(int poz_x, int poz_y, int sir_table, int vis_table)
{
	Crtica k;
	k.x = k.y = k.i = -1;
	if (poz_x%2 && poz_y%2 || !(poz_x%2) && !(poz_y%2)) return k;
	if (poz_x%2)
	{
		k.x = poz_x/2;
		if ((poz_y+1)/2 < vis_table)
		{
			k.y = (poz_y+1)/2;
			k.i = 2;
		}
		else
		{
			k.y = (poz_y-1) / 2;
			k.i = 4;
		}
	}
	else
	{
		k.y = poz_y/2;
		if ((poz_x+1)/2 < sir_table)
		{
			k.x = (poz_x+1)/2;
			k.i = 1;
		}
		else
		{
			k.x = (poz_x-1) / 2;
			k.i = 3;
		}
	}
	return k;
}


/*Ispisuje zadati meni u prozoru menu_win,
* i ceka unos, pa zatim ispisuje odgovarajuci
* meni ili vraca vrednost na osnovu naredne
* akcije koju treba izvrsiti*/
int inic_meni(WINDOW* menu_win, int meni)
{
	int opcija;
	int x, y; //granice prozora
	getmaxyx(menu_win, y, x);
	while (1)
	{
		switch (meni)
		{
			case GLAVNI_MENI:
				opcija = w_meni(menu_win, glavni_meni, 7);
				switch (opcija)
				{
					case 1: //zapocni partiju protiv kompjutera
						return 0;
						break;
					case 2: //zapocni partiju protiv drugog igraca
						return 1;
						break;
					case 3:
						//nastavi partiju
						return 2;
						break;
					case 4:
						meni = HIGHSCORE;
						continue;
					case 5:
						meni = PODESAVANJA;
						continue;
					case 6:
						//outro_scr(stdscr, pozdrav)
						exit(0);
				}
				break;
			case HIGHSCORE:{
				/* ucitaj rezultate u niz rezultati
				*  w_meni(menu_win, rezultati, 10, 0, x, y);
				*  meni = GLAVNI_MENI;
				*  continue;*/ 
				meni = GLAVNI_MENI;
				break;}
			case PODESAVANJA:
				opcija = w_meni(menu_win, podesavanja, 5);
				switch (opcija)
				{
					case 1:
						meni = TEZINA;
						continue;
					case 2:
						meni = VELICINA_MAPE;
						continue;
					case 3:
						meni = VRSTA_MAPE;
						continue;
					case 4:
						meni = GLAVNI_MENI;
						continue;
				}
			case TEZINA:{

				opcija = w_meni(menu_win, tezine, 5);
				if (opcija != 4) tezina = opcija;
				meni = PODESAVANJA;
				continue;}
			case VELICINA_MAPE:
				if (kvadratna) 
				{	opcija = w_meni(menu_win, velicina_kv, 9);
					switch(opcija)
					{
						case 1: visina = 5; sirina = 5; break;
						case 2: visina = 5; sirina = 10; break;
						case 3: visina = 5; sirina = 15; break;
						case 4: visina = 10; sirina = 10; break;
						case 5: visina = 10; sirina = 15; break;
						case 6: visina =15; sirina = 15; break;
					}
				}
				else 
				{	opcija = w_meni(menu_win, velicina_tr, 6);
					if (opcija != 5) mapa_tr = opcija;}
				meni = PODESAVANJA;
				continue;
			case VRSTA_MAPE:
				opcija = w_meni(menu_win, vrsta_mape, 4);
				if (opcija != 3) kvadratna = opcija -1;
				meni = PODESAVANJA;
				continue;
			case PAUZA:
				opcija = w_meni(menu_win, pauza, 6);
				switch (opcija)
				{
					case 1:
						//vrati na prozor za partiju
						break;
					case 2:
						//save
						continue;
					case 3:
						//nastavi sacuvanu partiju
						break;
					case 4:
						//outro_scr(stdscr, pozdrav);
						exit(0);
				}
		}
	}
}

int cekaj_potez(WINDOW* tabla, int igrac, short** table)
{
	Sused polja;
	int vis = ((*tabla)._maxy-5)/2, sir = ((*tabla)._maxx-5)/2;
	int promena = 0;
	int dugme;
	Crtica crt;
	static MEVENT klik;

	keypad(tabla, TRUE);
	while ((dugme = wgetch(tabla)) == KEY_MOUSE)
	{
		if (dugme == KEY_MOUSE && nc_getmouse(&klik) == OK)
		{
			if (klik.bstate & BUTTON1_CLICKED)
			{
				crt = koord_crtice(klik.x - (*tabla)._begx - 2, klik.y-(*tabla)._begy-2, sir, vis);
				if (crt.i == -1) continue;
				mvwchgat(tabla, klik.y-(*tabla)._begy, klik.x-(*tabla)._begx, 1, A_BOLD|A_ALTCHARSET, 4, NULL);				if (odigraj_potez_kv(table, 5, 5, crt.y, crt.x, crt.i))
				{
				switch (crt.i)
				{
					case 1:
						if (crt.x && table[crt.y][crt.x-1] == 2222)
						{
							mvwchgat(tabla, crt.y*2 +3, (crt.x-1)*2 +3, 1, A_BOLD|A_ALTCHARSET, igrac, NULL);
							if (igrac) igrac2++;
							else igrac1++;
							osvojena_polja++;
							promena++;
						}
						break;
					case 2:
						if (crt.y && table[crt.y-1][crt.x] == 2222)
						{
							mvwchgat(tabla, (crt.y-1)*2 +3, crt.x*2 +3, 1, A_BOLD|A_ALTCHARSET, igrac, NULL);
							if (igrac) igrac2++;
							else igrac1++;
							osvojena_polja++;
							promena++;
						}
						break;
				}
				if (table[crt.y][crt.x] == 2222)
				{
					mvwchgat(tabla, crt.y*2 +3, crt.x*2 +3, 1, A_BOLD|A_ALTCHARSET, igrac, NULL);
					if (igrac) igrac2++;
					else igrac1++;
					osvojena_polja++;
					promena++;
				}
				return !promena;
				}
			}
		}
	}
}