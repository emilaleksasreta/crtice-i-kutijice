#include "Odigravanje poteza.h"




short levoKv(short n)
{
	return n / 1000;
}

short goreKv(short n)
{
	return (n / 100) % 10;
}

short desnoKv(short n)
{
	return (n / 10) % 10;
}

short doleKv(short n)
{
	return n % 10;
}

short levoTr(short n)
{
	return n / 100;
}

short gore_doleTr(short n)
{
	return (n / 10) % 10;
}

short desnoTr(short n)
{
	return n % 10;
}


Sused sused_kvadrat(int i, int j, int m, int n)
{
	Sused susedi;

	if (j-1 >= 0) 
		susedi.levo.i = i, susedi.levo.j = j-1;
	else 
		susedi.levo.i = -1;
	
	if (i-1 >= 0) 
		susedi.gore.i = i-1, susedi.gore.j = j;
	else 
		susedi.gore.i = -1;
	
	if (j+1 < n)
		susedi.desno.i = i, susedi.desno.j = j+1;
	else
		susedi.desno.i = -1;

	if (i+1 < m)
		susedi.dole.i = i+1, susedi.dole.j = j;
	else
		susedi.dole.i = -1;
	
	return susedi;
}

Sused sused_trougao(int i, int j, int m)
{
	Sused susedi;

	if (j-1 >= 0)
		susedi.levo.i = i, susedi.levo.j = j-1;
	else
		susedi.levo.i = -1;

	if (j+1 <= i*2)
		susedi.desno.i = i, susedi.desno.j = j+1;
	else
		susedi.desno.i = -1;

	if (j%2 == 0)
	{
		susedi.gore.i = -1;
		if (i+1 < m)
			susedi.dole.i = i+1, susedi.dole.j = j+1;
		else
			susedi.dole.i = -1;
	}
	else
	{
		susedi.dole.i = -1;
		if (i-1 >= 0)
			susedi.gore.i = i-1, susedi.gore.j = j-1;
		else
			susedi.gore.i = -1;
	}

	return susedi;
}


short** kvadratna_tabla(int m, int n)
{
	short **tablaKv;
	int i, j;

	tablaKv = (short**)malloc(m*sizeof(short*));
	for(i=0; i<m; i++)
		tablaKv[i] = (short*)malloc(n*sizeof(short));

	for(i=0; i<m; i++)
		for(j=0; j<n; j++)
			tablaKv[i][j] = 1111;
	
	return tablaKv;
}

short** trougaona_tabla(int m)
{
	short **tablaTr;
	int i, j;

	tablaTr = (short**)malloc(m*sizeof(short*));
	for (i=0; i<m; i++)
		tablaTr[i] = (short*)malloc((i*2+1)*sizeof(short)); //U svakoj vrsti ima i*2+1 kolona

	for(i=0; i<m; i++)
		for(j=0; j<(i*2+1); j++)
			tablaTr[i][j] = 111;

	return tablaTr;
}


/*Funkcija koja menja stanje kvadratne matrice kad se odigra potez.
Prima kvadratnu matricu, dimenzije, koordinate polja i broj od 1 do 4 koji oznacava stranicu polja
Vraca 1 ako je bilo moguce odigrati potez, a 0 ako je nemoguce.*/
int odigraj_potez_kv(short **tabla, int m, int n, int i, int j, int str)
{
	Sused susedi;

	susedi = sused_kvadrat(i, j, m, n);

	switch(str)
	{
		case 1:
			if (levoKv(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 1000;
			if (susedi.levo.i != -1)
				tabla[susedi.levo.i][susedi.levo.j] += 10;
			break;
		case 2:
			if (goreKv(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 100;
			if (susedi.gore.i != -1)
				tabla[susedi.gore.i][susedi.gore.j] += 1;
			break;
		case 3:
			if (desnoKv(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 10;
			if (susedi.desno.i != -1)
				tabla[susedi.desno.i][susedi.desno.j] += 1000;
			break;
		case 4:
			if (doleKv(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 1;
			if (susedi.dole.i != -1)
				tabla[susedi.dole.i][susedi.dole.j] += 100;
			break;
	}
	return 1;
}

/*Funkcija koja menja stanje trougaone matrice kad se odigra potez.
Prima trougaonu matricu, dimenziju, koordinate polja i broj od 1 do 3 koji oznacava stranicu polja
Vraca 1 ako je bilo moguce odigrati potez, a 0 ako je nemoguce.*/
int odigraj_potez_tr(short **tabla, int m, int i, int j, int str)
{
	Sused susedi;

	susedi = sused_trougao(i, j, m);

	switch(str)
	{
		case 1:
			if (levoTr(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 100;
			if (susedi.levo.i != -1)
				tabla[susedi.levo.i][susedi.levo.j] += 1;
			break;
		case 2:
			if (gore_doleTr(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 10;
			if (susedi.dole.i != -1)
				tabla[susedi.dole.i][susedi.dole.j] += 10;
			else if (susedi.gore.i != -1)
				tabla[susedi.gore.i][susedi.gore.j] += 10;
			break;
		case 3:
			if (desnoTr(tabla[i][j]) == 2)
				return 0;
			else tabla[i][j] += 1;
			if (susedi.desno.i != -1)
				tabla[susedi.desno.i][susedi.desno.j] += 100;
			break;
	}
	return 1;
}
	

//Testiranje
/*void main()
{
	short **tablaKv, **tablaTr;
	int m, n, i, j, str;

	printf("Uneti dimenzije kvadratne\n");
	scanf("%d%d", &m, &n);
	tablaKv = kvadratna_tabla(m, n);

	for(i=0; i<m; i++)
	{
		for(j=0; j<n; j++)
			printf("%hd ", tablaKv[i][j]);
		printf("\n");
	}

	printf("Igranje poteza KVADRAT:\nUnositi koordinate i stranicu za promenu stanja matrice.\nLos unos za kraj!\n");
	printf("\n");
	scanf("%d%d%d", &i, &j, &str);
	while (i>=0 && j>=0 && i<m && j<n && str>=1 && str<=4)
	{
		if (odigraj_potez_kv(tablaKv, m, n, i, j, str) == 0) //Menjanje matrice
			printf("Nemoguc potez!\n");
		else printf("Potez odigran!\n");
		for(i=0; i<m; i++) //Stampanje
			{
				for(j=0; j<n; j++)
					printf("%hd ", tablaKv[i][j]);
				printf("\n");
			}
		printf("\n");
		scanf("%d%d%d", &i, &j, &str);
	}

	printf("==============================\n");

	printf("Uneti dimenziju trougaone\n");
	scanf("%d", &m);
	tablaTr = trougaona_tabla(m);

	for(i=0; i<m; i++)
	{
		for(j=0; j<(i*2+1); j++)
			printf("%hd ", tablaTr[i][j]);
		printf("\n");
	}

	printf("Igranje poteza TROUGAO:\nUnositi koordinate i stranicu za promenu stanja matrice.\nLos unos za kraj.\n");
	printf("\n");
	scanf("%d%d%d", &i, &j, &str);
	while (i>=0 && j>=0 && i<m && j<2*i+1 && str>=1 && str<=3)
	{
		if (odigraj_potez_tr(tablaTr, m, i, j, str) == 0) //Menjanje matrice
			printf("Nemoguc potez!\n");
		else printf("Potez odigran!\n");

		for(i=0; i<m; i++) //Stampanje
			{
				for(j=0; j<(i*2+1); j++)
					printf("%hd ", tablaTr[i][j]);
				printf("\n");
			}
		printf("\n");
		scanf("%d%d%d", &i, &j, &str);
	}
}*/